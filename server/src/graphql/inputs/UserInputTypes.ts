import { MinLength } from "class-validator";
import { Field, InputType } from "type-graphql";

@InputType()
export default class UserRegister {
  @Field(() => String, {
    nullable: false,
  })
  id: string;

  @Field(() => String, {
    nullable: false,
  })
  @MinLength(6)
  username: string;

  @Field(() => String, {
    nullable: false,
  })
  @MinLength(6)
  email: string;

  @Field(() => String, {
    nullable: true,
  })
  imageUrl: string | null;

  @Field(() => Date, {
    nullable: false,
  })
  createdAt: Date;

  @Field(() => Date, {
    nullable: false,
  })
  updatedAt: Date;
}
