import { Resolver, Query, Ctx, Mutation, Arg } from "type-graphql";
import { MyContext } from "../../types";
import User from "../models/User";
import argon2 from "argon2";

@Resolver(() => User)
export default class UserResolver {
  @Query(() => [User])
  async users(@Ctx() { prisma }: MyContext): Promise<User[]> {
    return await prisma.user.findMany();
  }

  @Mutation(() => User)
  async register(
    @Ctx() { prisma }: MyContext,
    @Arg("username") username: string,
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Arg("confirmPassword") confirmPassword: string,
  ): Promise<User> {
    try {
      // TODO: Validate input data
      // TODO: Check if username/email exists

      // TODO: Hash problem
      const hashedPassword = await argon2.hash(password);

      // TODO: Create user
      const user = await prisma.user.create({
        data: {
          username,
          email,
          password: hashedPassword,
        },
      });

      // TODO: Return user
      return user;
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}
