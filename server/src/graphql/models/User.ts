import { Field, ObjectType } from "type-graphql";

@ObjectType()
export default class User {
  @Field(() => String, {
    nullable: false,
  })
  id: string;

  @Field(() => String, {
    nullable: false,
  })
  username: string;

  @Field(() => String, {
    nullable: false,
  })
  email: string;

  @Field(() => String, {
    nullable: true,
  })
  imageUrl: string | null;

  @Field(() => Date, {
    nullable: false,
  })
  createdAt: Date;

  @Field(() => Date, {
    nullable: false,
  })
  updatedAt: Date;
}
