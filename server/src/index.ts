import { ApolloServer } from "apollo-server";
import "reflect-metadata";
import { prisma } from "./database";
import { buildSchema } from "type-graphql";
import UserResolver from "./graphql/resolvers/UserResolver";
import { MyContext } from "./types";

(async () => {
  // Apollo
  const server = new ApolloServer({
    schema: await buildSchema({
      resolvers: [UserResolver],
      validate: false,
    }),
    context: () => ({ prisma } as MyContext),
  });

  server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  });
})();
